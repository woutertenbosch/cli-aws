FROM python:3.8.5-buster
RUN apt update && apt install zip && pip install awscli && rm -rf /var/lib/apt/lists/*
